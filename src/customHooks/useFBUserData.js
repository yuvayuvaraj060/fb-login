export const useFBUserData = () => {
  const userData = JSON.parse(localStorage.getItem("userData"));
  console.log(window.FB)
  const accessToken = window?.FB?.getAccessToken();
  const authResponse = window?.FB?.getAuthResponse();
  const userID = window?.FB?.getUserID();
  const logout = () => window?.FB?.logout();
  const email = userData.email;
  const name = userData.name;
  const img = userData.img;

  return {
    accessToken,
    authResponse,
    userID,
    logout,
    email,
    name,
    img,
  };
};
