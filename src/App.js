import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { Login } from "./components/Login.js";
import Home from "./components/navbar/NavBar.js";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <Route exact path="/login" render={() => <Login />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
