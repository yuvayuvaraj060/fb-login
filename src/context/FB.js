import React, { createContext } from "react";
import FBProvider from "react-facebook-login/dist/facebook-login-with-button";
const loadFBSdk = createContext();
export const FB = ({ children }) => {
  return <loadFBSdk.Provider value={FBProvider()}>{children}</loadFBSdk.Provider>;
};
