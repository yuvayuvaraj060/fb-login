import React from "react";
import { useFBUserData } from "../../customHooks/useFBUserData";
const NavBar = () => {
  const { img, name, logout } = useFBUserData();
  return (
    <nav>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "center",
          gap: "15px",
          padding: "20px",
          backgroundColor: "black",
        }}
      >
        <div style={{ color: "white", fontSize: "20px" }}>{name}</div>
        <img
          style={{ borderRadius: "50px" }}
          src={img.data.url}
          alt="userProfile"
        />
        <button
          style={{
            border: "0",
            borderColor: "white",
            color: "black",
            fontSize: "20px",
            cursor: "pointer",
          }}
          onClick={() => logout()}
        >
          logout
        </button>
      </div>
    </nav>
  );
};

export default NavBar;
