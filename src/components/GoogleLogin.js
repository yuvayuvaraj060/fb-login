import React from "react";
import { GoogleLogin } from "react-google-login";
export const GoogleLoginButton = () => {
  const responseGoogle = (response) => {
    console.log(response);
  };
  return (
    <div>
      <GoogleLogin
        clientId="1028346427798-icj7g7fi517eq6cvunt4vu4hg9uov23v.apps.googleusercontent.com"
        buttonText="Login"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
};
