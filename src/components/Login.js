import React from "react";
import { FBLogin } from "./FBLogin.js";
import { GoogleLoginButton } from "./GoogleLogin.js";
export const Login = () => {
  return (
    <div style={{ display: "grid", placeItems: "center", height: "100vh" }}>
      <div style={{ display: "flex", gap: "20px" }}>
        <FBLogin />
        <GoogleLoginButton />
      </div>
    </div>
  );
};
