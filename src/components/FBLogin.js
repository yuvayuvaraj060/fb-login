import React from "react";
import FacebookLogin from "react-facebook-login";
import { useHistory } from "react-router-dom";

export const FBLogin = () => {
  const history = useHistory();
  const responseFromFacebook = (response) => {
    console.log(
      "🚀 ~ file: FBLogin.js ~ line 8 ~ responseFromFacebook ~ response",
      response
    );
    const userObj = {
      email: response.email,
      name: response.name,
      img: response.picture,
    };
    localStorage.setItem("userData", JSON.stringify(userObj));
    history.push("/");
  };
  return (
    <div>
      <FacebookLogin
        appId="540943797062433"
        autoLoad={true}
        fields="name,email,picture"
        callback={responseFromFacebook}
        cookie={true}
      />
    </div>
  );
};
